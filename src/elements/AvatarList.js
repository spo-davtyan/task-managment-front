import { Close } from "@material-ui/icons";
import ReactTooltip from 'react-tooltip';
import { getAvatarUrl } from "../helpers/url";

function AvatarList({ isShow, memberOnClick, members }) {
    const tooltipContent = (member) => {
        return `
            ${member.name} <br />
            ${member.email}
        `
    }

    return <div className='AvatarWrapper' >
        {members.map((member) => {
            return <div key={member.id} className='Avatar'
                data-tip={tooltipContent(member)}
                style={{
                    backgroundImage: member.hasAvatar ? `url(${getAvatarUrl(member.id)})` : 'none'
                }}>
                {!member.hasAvatar && member.name.charAt(0).toUpperCase()}
                {isShow && <Close color='secondary' fontSize='large'
                    className='hover Close' onClick={() => {
                        memberOnClick(member.id)
                    }} />}
            </div>
        })}
        <ReactTooltip effect='solid' html={true} getContent={(data) => data} />
    </div>
}
export default AvatarList