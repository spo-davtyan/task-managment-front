import { connect } from "react-redux";
import { useState } from "react";
import { Menu, MenuItem, Box, IconButton } from '@material-ui/core';
import { AccountCircle } from '@material-ui/icons';
import axios from "./../helpers/axios";
import { removeUser } from '../redux/user/actions';
import { getUser } from '../redux/user/selector';
import {
    Link
} from "react-router-dom";
function ProfilMenu(props) {
    const [anchorEl, setAnchorEl] = useState(null);
    const verify = () => {
        setAnchorEl(null);
        axios.post('/verify', {
            token: props.user.refresh
        }).then((user) => {
            return user
        }).catch((error) => {
        })
    }
    const logout = () => {
        setAnchorEl(null);
        props.removeUser()
    }
    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };
    return <Box>
        <IconButton
            aria-label="account of current user"
            aria-controls="menu-appbar"
            aria-haspopup="true"
            onClick={handleClick}
            color="inherit"
        >
            <AccountCircle />
        </IconButton>
        <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={!!anchorEl}
            onClose={handleClose}
        >
            <MenuItem onClick={handleClose}><Link to={'/profile'}>Profile</Link></MenuItem>
            <MenuItem onClick={verify}>Verify</MenuItem>
            <MenuItem onClick={logout}>Logout</MenuItem>
        </Menu>
    </Box>
}
const state = state => ({ user: getUser(state) })
const func = dispatch => ({
    removeUser: () => dispatch(removeUser()),
})

export default connect(state, func)(ProfilMenu);