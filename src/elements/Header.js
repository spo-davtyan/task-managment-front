import { Toolbar, AppBar, Box, Typography, makeStyles } from '@material-ui/core';
import ProfilMenu from "./ProfilMenu";

import {
    Link, useParams
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    box: {
        display: 'flex',
        'align-items': 'center',
        width: '100%'
    },
    title: {
        flexGrow: 1,
    },
    link: {
        color: '#fff',
        cursor: 'pointer',
        'text-decoration': 'none',
        '&:hover': {
            'text-decoration': 'none',
        }
    },
}));

function Header(params) {
    const classes = useStyles()
    const { id } = useParams()
    return (
        <header>
            <AppBar position={'static'}>
                <Toolbar>
                    <Box className={classes.box}>
                        <Typography variant="h6" component="h1" className={classes.title}>
                            <Link className={classes.link} to={'/home'}>ARIK</Link>
                        </Typography>
                        {id && <Link className={classes.link} to={`/${id}/docs`}>Documentation</Link>}
                        <ProfilMenu />
                    </Box>
                </Toolbar>
            </AppBar>
        </header>
    );
}
export default Header