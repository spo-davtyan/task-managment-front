export const getUser = ({ users }) => users.user;
export const isLoggedin = ({ users }) => {
    return Boolean(users.user)
};