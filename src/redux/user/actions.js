import { LOAD_USER, LOGOUT_USER, UPDATE_USER_INFO, UPDATE_USER_PASSWORD } from "./actionTypes";
import axios from "./../../helpers/axios";
import { RESET_PROJECTS } from "../project/actionTypes";

export const sendEmail = (email) => {
    return axios.post('/email/verify', {
        email,
    }).then((user) => {
        return user
    })
};

export const verifyAccount = (email, token, name, password) => dispatch => {
    return axios.post('/verify/account', {
        email, token, name, password
    }).then((user) => {
        dispatch({
            type: LOAD_USER,
            user
        })
        return user
    })
};

export const loadUser = (email, password) => dispatch => {
    return axios.post('/login', {
        email,
        password
    }).then((user) => {
        dispatch({
            type: LOAD_USER,
            user
        })
        return user
    })
};
export const updateUserAvatar = (data) => dispatch => {
    return axios({
        url: '/user/update/avatar',
        data,
        method: 'POST',
        headers: { 'Content-Type': 'multipart/form-data' }
    }).then(() => {
        dispatch({
            type: UPDATE_USER_INFO,
            user: {
                hasAvatar: true
            }
        })
    })
};

export const updateUserName = (name) => dispatch => {
    return axios.post('/user/update/name', {
        name
    }).then((user) => {
        dispatch({
            type: UPDATE_USER_INFO,
            user
        })
        return user
    })
};

export const updateUserPassword = (current, confirm, password) => dispatch => {
    return axios.post('/user/update/password', {
        current, confirm, password
    }).then(() => {
        dispatch({
            type: UPDATE_USER_PASSWORD
        })
        return true
    })
};

export const removeUser = () => dispatch => {

    dispatch({
        type: LOGOUT_USER
    })

    dispatch({
        type: RESET_PROJECTS
    })
};

