import { LOAD_USER, LOGOUT_USER, UPDATE_USER_INFO } from "./actionTypes";

const getUserFromStorage = () => {
    const user = localStorage.getItem('user')
    return user ? JSON.parse(user) : null
};

const setUserToStorage = (user) => {
    localStorage.setItem('user', JSON.stringify(user))
};
const deleteUserFromStorage = () => {
    localStorage.removeItem('user')
};

const initialState = {
    user: getUserFromStorage()
};
export function users(state = initialState, action) {
    switch (action.type) {
        case LOAD_USER: {
            const { user } = action;
            setUserToStorage(user)
            return {
                ...state,
                user,
            };
        }
        case UPDATE_USER_INFO: {
            const user = {
                ...state.user,
                ...action.user
            }
            setUserToStorage(user)
            return {
                ...state,
                user,
            };
        }
        case LOGOUT_USER: {
            deleteUserFromStorage()
            return {
                ...state,
                user: null,
            };
        }
        default:
            return state;
    }
}
