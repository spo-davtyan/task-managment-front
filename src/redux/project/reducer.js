import { removeListItem } from "../../helpers/reorderList";
import {
    LOAD_USER_PROJECTS, RESET_PROJECTS, CREATE_PROJECT, LOAD_PROJECT, CREATE_TASK,
    UPDATE_COLUMN_ORDER, REMOVE_COLUMN, UPDATE_COLUMN_NAME,
    CREATE_COLUMN, REMOVE_TASK, UPDATE_TASK_ORDER, REMOVE_PROJECT,
    MOVE_TASK, UPDATE_TASK_NAME, UPDATE_TASK_DESC, TASK_ADD_MEMBER, TASK_REMOVE_MEMBER, UPDATE_PROJECT_NAME, UPDATE_PROJECT_TAGS
} from "./actionTypes";

const initialState = {
    projects: [],
    project: null,
};

export function projects(state = initialState, action) {
    switch (action.type) {
        case RESET_PROJECTS: {
            return {
                projects: [],
            };
        }
        case UPDATE_PROJECT_NAME: {
            const { projectId, name } = action;

            const projects = state.projects.map(project => {

                if (project.id === projectId) {
                    return {
                        ...project,
                        name
                    }
                }
                return project
            })
            return {
                ...state,
                projects,
                project: {
                    ...state.project,
                    name
                }
            }
        }
        case UPDATE_PROJECT_TAGS: {
            const { tags } = action;

            return {
                ...state,
                project: {
                    ...state.project,
                    tags
                }
            }
        }
        case REMOVE_PROJECT: {
            const { projectId } = action;
            const projects = state.projects.filter(p => p.id !== projectId)
            return {
                ...state,
                projects,
                project: null
            };
        }
        case CREATE_PROJECT: {
            const { project } = action;
            return {
                ...state,
                projects: [
                    ...state.projects,
                    project
                ],
            };
        }
        case REMOVE_COLUMN: {
            const { columnId } = action;
            let { columns } = state.project

            removeListItem(columns, columnId)
            return {
                ...state,
                project: {
                    ...state.project,
                    columns: [
                        ...columns
                    ]
                },
            };
        }
        case UPDATE_COLUMN_NAME: {
            const { columnId, name } = action;
            const project = state.project
            let { columns } = project
            columns = columns.map(column => {
                if (column.id === columnId) {

                    return {
                        ...column,
                        name
                    }
                }
                return column
            })
            return {
                ...state,
                project: {
                    ...project,
                    columns
                }
            };
        }
        case CREATE_COLUMN: {
            const { column } = action;
            const project = state.project
            const { columns } = project

            return {
                ...state,
                project: {
                    ...project,
                    columns: [
                        ...columns,
                        column
                    ]
                }
            }

        }
        case CREATE_TASK: {
            const { columnId, task } = action;
            const project = state.project
            const { columns } = project
            const newColumns = columns.map(column => {
                if (column.id === columnId) {
                    return {
                        ...column,
                        tasks: [
                            task,
                            ...column.tasks,
                        ]
                    }
                }
                return column
            })
            return {
                ...state,
                project: {
                    ...project,
                    columns: [
                        ...newColumns
                    ]
                }
            }

        }
        case REMOVE_TASK: {
            const { columnId, taskId } = action;
            const { columns } = state.project

            const newColumns = columns.map(column => {
                if (column.id === columnId) {
                    let { tasks } = column

                    removeListItem(tasks, taskId)

                    return {
                        ...column,
                        tasks: [...tasks]
                    }
                }
                return column
            })

            const project = {
                ...state.project,
                columns: [
                    ...newColumns
                ]
            }
            return {
                ...state,
                project: {
                    ...project
                },
            };
        }
        case UPDATE_TASK_DESC: {
            const { columnId, taskId, description } = action;
            const project = state.project
            let { columns } = project
            columns = columns.map(column => {
                if (column.id === columnId) {
                    let { tasks } = column
                    return {
                        ...column,
                        tasks: tasks.map(task => {
                            if (task.id === taskId) {
                                return {
                                    ...task,
                                    description
                                }
                            }
                            return task
                        })
                    }
                }
                return column
            })
            return {
                ...state,
                project: {
                    ...project,
                    columns
                }
            };
        }
        case UPDATE_TASK_NAME: {
            const { columnId, taskId, name } = action;
            const project = state.project
            let { columns } = project
            columns = columns.map(column => {
                if (column.id === columnId) {
                    let { tasks } = column
                    return {
                        ...column,
                        tasks: tasks.map(task => {
                            if (task.id === taskId) {
                                return {
                                    ...task,
                                    name
                                }
                            }
                            return task
                        })
                    }
                }
                return column
            })
            return {
                ...state,
                project: {
                    ...project,
                    columns
                }
            };
        }
        case UPDATE_COLUMN_ORDER: {
            const { columns } = action

            return {
                ...state,
                project: {
                    ...state.project,
                    columns: [
                        ...columns
                    ]
                }
            }
        }
        case UPDATE_TASK_ORDER: {
            const { columnId, tasks } = action
            const project = state.project
            const { columns } = project
            const reorderedColumns = columns.map(column => {
                if (column.id === columnId) {
                    return {
                        ...column,
                        tasks: [
                            ...tasks
                        ]
                    }
                }
                return column
            })

            return {
                ...state,
                project: {
                    ...project,
                    columns: [
                        ...reorderedColumns
                    ]
                },
            }
        }
        case TASK_ADD_MEMBER: {
            const { columnId, taskId, userId } = action;
            const project = state.project
            let { columns } = project
            columns = columns.map(column => {
                if (column.id === columnId) {
                    let { tasks } = column
                    return {
                        ...column,
                        tasks: tasks.map(task => {
                            if (task.id === taskId) {
                                return {
                                    ...task,
                                    members: [
                                        ...task.members,
                                        userId
                                    ]
                                }
                            }
                            return task
                        })
                    }
                }
                return column
            })
            return {
                ...state,
                project: {
                    ...project,
                    columns
                }
            };
        }
        case TASK_REMOVE_MEMBER: {
            const { columnId, taskId, userId } = action;
            const project = state.project
            let { columns } = project
            columns = columns.map(column => {
                if (column.id === columnId) {
                    let { tasks } = column
                    return {
                        ...column,
                        tasks: tasks.map(task => {
                            if (task.id === taskId) {
                                const newMembers = task.members.filter(memberId => memberId !== userId)
                                return {
                                    ...task,
                                    members: [
                                        ...newMembers
                                    ]
                                }
                            }
                            return task
                        })
                    }
                }
                return column
            })
            return {
                ...state,
                project: {
                    ...project,
                    columns
                }
            };
        }
        case MOVE_TASK: {
            const { sourceColumnId, sourceTask, destColumnId, destTask } = action
            const project = state.project
            const { columns } = project

            const reorderedColumns = columns.map(column => {
                const isSourceColumnId = column.id === sourceColumnId

                if (isSourceColumnId || column.id === destColumnId) {
                    const tasks = isSourceColumnId ? sourceTask : destTask
                    return {
                        ...column,
                        tasks: [
                            ...tasks
                        ]
                    }
                }

                return column
            })

            return {
                ...state,
                project: {
                    ...project,
                    columns: reorderedColumns
                }
            }
        }
        case LOAD_USER_PROJECTS: {
            const { projects } = action;
            return {
                ...state,
                projects,
            };
        }
        case LOAD_PROJECT: {
            const { project } = action;
            return {
                ...state,
                project,
            };
        }

        default:
            return state;
    }
}
