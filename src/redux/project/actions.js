import {
    LOAD_USER_PROJECTS, CREATE_PROJECT, REMOVE_PROJECT,
    LOAD_PROJECT, CREATE_COLUMN, CREATE_TASK,
    MOVE_TASK, UPDATE_COLUMN_ORDER, UPDATE_TASK_ORDER, UPDATE_PROJECT_TAGS,
    UPDATE_COLUMN_NAME, UPDATE_TASK_NAME, UPDATE_TASK_DESC,
    TASK_ADD_MEMBER, UPDATE_PROJECT_NAME
} from "./actionTypes";
import axios from "./../../helpers/axios";
import { reorderList } from "../../helpers/reorderList";
import { UPDATE_USER_INFO } from "../user/actionTypes";
import { v4 as uuidv4 } from 'uuid';


// PROJECT
export const createProject = (userProjectsList) => dispatch => {
    return axios.post(`/projects/create`, {
        name: `Untitled Project`,
    })
        .then((project) => {
            dispatch({
                type: CREATE_PROJECT,
                project
            })
            const projects = [
                ...userProjectsList,
                project.id
            ]

            dispatch({
                type: UPDATE_USER_INFO,
                user: {
                    projects
                }
            })


            return project
        })
};
export const deleteProject = (projectId, userProjects) => dispatch => {
    return axios.delete(`/projects/${projectId}`)
        .then(() => {
            dispatch({
                type: REMOVE_PROJECT,
                projectId
            })
            const projects = userProjects.filter(p => p !== projectId)

            dispatch({
                type: UPDATE_USER_INFO,
                user: {
                    projects
                }
            })
            return true
        })
};

export const loadUserProjects = (ids) => dispatch => {
    const projectsIds = ids.join(',')
    return axios.get(`/projects?ids=${projectsIds}`)
        .then((projects) => {

            dispatch({
                type: LOAD_USER_PROJECTS,
                projects
            })
            return projects
        })
};

export const updateProjectName = (projectId, name) => dispatch => {
    return axios.patch(`/projects/${projectId}/updateName`, {
        name
    })
        .then(() => {
            dispatch({
                type: UPDATE_PROJECT_NAME,
                name,
                projectId
            })
            return name
        })
};
export const removeProjectTag = (projectId, tags, id) => {
    const newTags = tags.filter(tag => tag.id !== id)
    const socket = {
        type: UPDATE_PROJECT_TAGS,
        tags: newTags
    }
    return axios.patch(`/projects/${projectId}/tags`, {
        tags: newTags,
        socket
    })
        .then(() => {
            return tags
        })
};
export const addProjectTag = (projectId, tags, newTag) => {
    const newTags = [
        ...tags,
        { id: uuidv4(), ...newTag }
    ]
    const socket = {
        type: UPDATE_PROJECT_TAGS,
        tags: newTags
    }
    return axios.patch(`/projects/${projectId}/tags`, {
        tags: newTags,
        socket
    })
        .then(() => {
            return tags
        })
};

export const loadProject = (id) => dispatch => {
    return axios.get(`/projects/${id}`)
        .then((project) => {
            // const users = []
            // let i = 0
            // for (const [key, value] of Object.entries(project.users)) {
            //     i++
            //     users.push({
            //         id: key,
            //         access: value,
            //         name: `user${i}`,
            //         email: `user${i}@gmail.com`
            //     })
            // }
            // project.users = users

            dispatch({
                type: LOAD_PROJECT,
                project
            })
            return project
        })
};


// COLUMN
export const createColumn = (projectId) => {
    const socketData = {
        type: CREATE_COLUMN,
        projectId,
    }
    return axios.post(`/columns/create`, {
        projectId,
        name: `Untitled Columns`,
        socket: socketData
    })
        .then((column) => {
            // we dont dispatch CREATE_COLUMN action it will add 2 elements 1 here 2 at socket
            // dispatch({
            //     ...socketData,
            //     column
            // })
            return column
        })
};

export const updateColumnName = (projectId, columnId, name) => {
    const dispatchData = {
        type: UPDATE_COLUMN_NAME,
        projectId, columnId, name
    }
    return axios.patch(`/columns/${columnId}/updateName`, {
        projectId,
        name,
        socket: dispatchData
    })
        .then(() => {
            return true
        })
};

export const deleteColumn = (projectId, columnId) => {

    return axios.delete(`/projects/${projectId}/columns/${columnId}`)
        .then(() => {
            return true
        })
};

export const reorderColumn = (project, startIndex, endIndex) => {
    const projectId = project.id
    reorderList(project.columns, startIndex, endIndex)
    const ids = project.columns.map(c => c.id)

    const dispatchData = {
        type: UPDATE_COLUMN_ORDER,
        columns: project.columns,
        projectId
    }
    return axios.patch(`/projects/${projectId}/columns/reorder`, {
        columns: ids,
        socket: dispatchData
    })
        .then(() => {
            // dispatch(dispatchData)
            return true
        })
};


// TASK
export const createTask = (projectId, columnId, name = `Untitled Task`) => {
    const socketData = {
        type: CREATE_TASK,
        projectId,
        columnId
    }
    return axios.post(`/tasks/create`, {
        columnId,
        name,
        socket: socketData
    })
        .then((task) => {
            // we dont dispatch CREATE_TASK action it will add 2 elements 1 here 2 at socket
            // dispatch({
            //     type: CREATE_TASK,
            //     columnId,
            //     task
            // })
            return task
        })
};

export const createTaskWithContent = (projectId, columnId, name) => {
    const socketData = {
        type: CREATE_TASK,
        projectId,
        columnId,
    }
    return axios.post(`/tasks/create`, {
        columnId,
        name,
        socket: socketData
    })
        .then((task) => {
            return task
        })
};

export const deleteTask = (projectId, columnId, taskId) => {
    return axios.delete(`projects/${projectId}/columns/${columnId}/tasks/${taskId}`)
        .then((column) => {
            // dispatch({
            //     type: REMOVE_TASK,
            //     projectId,
            //     columnId,
            //     taskId
            // })
            return column
        })
};

export const updateTaskName = (projectId, columnId, taskId, name) => {
    const dispatchData = {
        type: UPDATE_TASK_NAME,
        projectId, columnId, taskId, name
    }
    return axios.patch(`/tasks/${taskId}/updateName`, {
        name,
        socket: dispatchData
    })
        .then(() => {
            // dispatch(dispatchData)
            return true
        })
};
export const addTaskMember = (projectId, columnId, taskId, userId) => {
    const dispatchData = {
        type: TASK_ADD_MEMBER,
        projectId, columnId, taskId, userId
    }
    return axios.patch(`/tasks/${taskId}/assign`, {
        userId,
        socket: dispatchData
    })
        .then(() => {
            return true
        })
};

export const removeTaskMember = (projectId, columnId, taskId, userId) => {
    return axios.delete(`/projects/${projectId}/columns/${columnId}/tasks/${taskId}/user/${userId}`)
        .then(() => {
            return true
        })
};
export const updateTaskDescription = (projectId, columnId, taskId, description) => {
    const dispatchData = {
        type: UPDATE_TASK_DESC,
        projectId, columnId, taskId, description
    }
    return axios.patch(`/tasks/${taskId}/updateDescription`, {
        description,
        socket: dispatchData
    })
        .then(() => {
            // dispatch(dispatchData)
            return true
        })
};


export const reorderTask = (projectId, column, startIndex, endIndex) => {

    reorderList(column.tasks, startIndex, endIndex)
    const ids = column.tasks.map(t => t.id)

    const dispatchData = {
        type: UPDATE_TASK_ORDER,
        tasks: column.tasks,
        columnId: column.id,
        projectId
    }
    return axios.patch(`/columns/${column.id}/tasks/reorder`, {
        tasks: ids,
        socket: dispatchData
    })
        .then(() => {
            // dispatch(dispatchData)
            return true
        })
};

export const moveTask = (projectId, sourceColumnId, sourceTask, destColumnId, destTask) => {
    const dispatchData = {
        type: MOVE_TASK,
        projectId, sourceColumnId, sourceTask, destColumnId, destTask
    }
    return axios.patch(`/tasks/move`, {
        [sourceColumnId]: sourceTask.map(task => task.id),
        [destColumnId]: destTask.map(task => task.id),
        socket: dispatchData
    })
        .then(() => {
            // dispatch(dispatchData)
            return true
        })
};