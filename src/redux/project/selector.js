export const getUserProjects = store => store.projects.projects;
export const hasProject = (store) => store.projects.project !== null;
export const getProject = (store) => store.projects.project;
export const getProjectMembers = (store) => hasProject(store) ? getProject(store).users : [];
export const getProjectTags = (store) => hasProject(store) ? getProject(store).tags : [];
