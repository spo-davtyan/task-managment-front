import {
    CREATE_PAGE,
    REMOVE_PAGE,
    UPDATE_PAGE_CONTENT,
    LOAD_PAGES,
    LOAD_PAGE,
    UPDATE_PAGE_TITLE
} from "./actionTypes";

const initialState = {
    pages: [],
    page: null
};

export function pages(state = initialState, action) {
    switch (action.type) {
        case CREATE_PAGE: {
            const { page } = action
            return {
                pages: [
                    page,
                    ...state.pages
                ],
                page,
            };
        }
        case REMOVE_PAGE: {
            const { pageId } = action
            const page = pageId === (state.page && state.page.id) ? null : state.page
            return {
                pages: state.pages.filter(page => page.id !== pageId),
                page,
            };
        }
        case LOAD_PAGES: {
            const { pages } = action
            return {
                ...state,
                pages
            };
        }
        case UPDATE_PAGE_CONTENT: {
            const { content } = action
            return {
                ...state,
                page: {
                    ...state.page,
                    content
                }
            };
        }
        case UPDATE_PAGE_TITLE: {
            const { title } = action
            const { pages } = state
            return {
                ...state,
                pages: pages.map(page => {
                    if (page.id === state.page.id) {
                        return {
                            ...page,
                            title
                        }
                    }
                    return page
                }),
                page: {
                    ...state.page,
                    title
                }
            };
        }
        case LOAD_PAGE: {
            const { page } = action
            return {
                ...state,
                page
            };
        }
        default:
            return state;
    }
}
