import axios from "./../../helpers/axios";
import { CREATE_PAGE, LOAD_PAGES, LOAD_PAGE, UPDATE_PAGE_CONTENT, UPDATE_PAGE_TITLE, REMOVE_PAGE } from "./actionTypes";

export const createPage = (projectId) => dispatch => {
    return axios.post(`/projects/${projectId}/pages/create`, {
        content: '<h1>Untitled Page</h1>',
        title: `Untitled Page`,
    })
        .then((page) => {
            dispatch({
                type: CREATE_PAGE,
                page
            })
            return page
        })
};

export const removePage = (pageId) => dispatch => {
    return axios.delete(`/pages/${pageId}`)
        .then(() => {
            dispatch({
                type: REMOVE_PAGE,
                pageId
            })
            return true
        })
};
export const loadProjectPages = (projectId) => dispatch => {
    return axios.get(`/projects/${projectId}/pages`)
        .then((pages) => {
            dispatch({
                type: LOAD_PAGES,
                pages
            })
            return pages
        })
};

export const loadPage = (pageId) => dispatch => {
    return axios.get(`/pages/${pageId}`)
        .then((page) => {
            dispatch({
                type: LOAD_PAGE,
                page
            })
            return page
        })
};

export const updatePageContent = (pageId, content) => dispatch => {
    return axios.patch(`/pages/${pageId}/update`, {
        content
    })
        .then(() => {
            dispatch({
                type: UPDATE_PAGE_CONTENT,
                content,
            })
            return true
        })
};

export const updatePageTitle = (pageId, title) => dispatch => {
    return axios.patch(`/pages/${pageId}/update`, {
        title
    })
        .then(() => {
            dispatch({
                type: UPDATE_PAGE_TITLE,
                title,
            })
            return true
        })
};
export const deleteProject = (projectId, userProjects) => dispatch => {
    // return axios.delete(`/projects/${projectId}`)
    //     .then(() => {
    //         dispatch({
    //             type: REMOVE_PROJECT,
    //             projectId
    //         })
    //         const projects = userProjects.filter(p => p !== projectId)

    //         dispatch({
    //             type: UPDATE_USER_INFO,
    //             user: {
    //                 projects
    //             }
    //         })
    //         return true
    //     })
};