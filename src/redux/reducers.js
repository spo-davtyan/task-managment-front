import { combineReducers } from "redux";
import { users } from "./user/reducer";
import { projects } from "./project/reducer";
import { pages } from "./page/reducer";

const Reducer = {
    users,
    projects,
    pages
}
const redusers = combineReducers(Reducer);
export default redusers
