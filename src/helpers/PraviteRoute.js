import React from 'react'
import {
    Redirect,
    Route
} from "react-router-dom";
import { connect } from "react-redux";
import { isLoggedin } from '../redux/user/selector';
import { Dashboard } from './../pages/Dashboard'

export function PraviteRoute({ path, isLoggedin, Page, children }) {
    return <Route path={path} render={() => isLoggedin ? <Dashboard><Page>{children}</Page></Dashboard> : <Redirect to="/" />} />
}

const state = state => ({ isLoggedin: isLoggedin(state) })

export default connect(state, null)(PraviteRoute);