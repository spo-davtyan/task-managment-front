import axios from "axios";
import { toast } from 'react-toastify';
import store from "./../redux/store";
import { removeUser } from "./../redux/user/actions";
const service = axios.create({
    timeout: 20000,
    baseURL: 'http://localhost:3000'
});

service.interceptors.request.use(
    config => {
        setToken(config)
        return config;
    },
    error => {
        return Promise.reject(error);
    }
);
service.interceptors.response.use(
    data => {
        return data.data;
    },
    error => {
        console.log(error);
        if (error && error.response && error.response.status === 401) {
            store.dispatch(removeUser())
        }
        if (error && error.response && error.response.data) {
            toast.error(error.response.data.error)
        }
        if (error.message === "Network Error") {
            toast.error('Server not respound')
        }

        return Promise.reject(error.response);
    }
);

const setToken = (config) => {
    const { user } = store.getState().users
    if (user) {
        config.headers.common.Authorization = `Bearer ${user.access}`
        return
    }
    delete config.headers.common.Authorization
};
export default service