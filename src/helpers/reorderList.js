export const removeListItem = (list, id) => {
    const startIndex = list.findIndex(item => item.id === id)
    list.splice(startIndex, 1);
}

export const reorderList = (arr, startIndex, endIndex) => {
    const [removed] = arr.splice(startIndex, 1);
    arr.splice(endIndex, 0, removed);
}