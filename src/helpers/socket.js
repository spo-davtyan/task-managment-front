import React, { createContext } from 'react'
import io from 'socket.io-client';

const WebSocketContext = createContext(null)
const WS_BASE = 'localhost:3000'
export { WebSocketContext }

export default ({ children }) => {
    let socket;
    let ws;

    const sendChange = (type, changes) => {
        const change = {
            type,
            changes
        }
        socket.emit("change", JSON.stringify(change));
    }
    const subscribeToProject = (roomId) => {
        socket.emit("join", roomId);
    }
    const unSubscribeAtProject = (roomId) => {
        socket.emit("leave", roomId);
    }

    if (!socket) {
        socket = io.connect(WS_BASE, { path: '/spo' })

        ws = {
            socket: socket,
            subscribeToProject,
            unSubscribeAtProject,
            sendChange
        }
    }

    return (
        <WebSocketContext.Provider value={ws}>
            {children}
        </WebSocketContext.Provider>
    )
}