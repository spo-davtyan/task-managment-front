import { avatarPrefix } from "./constants"

export const getAvatarUrl = (id) => {
    return `${avatarPrefix}/${id}.png`
}