import { useState, useEffect } from 'react';

function getWindowDimensions() {
    const { innerWidth: width, innerHeight: height } = window;
    return {
        width,
        height
    };
}
let timer;
export default function useWindowDimensions() {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    useEffect(() => {
        function handleResize() {
            setWindowDimensions(getWindowDimensions());
        }

        function debounce() {
            if (timer) clearTimeout(timer);
            timer = setTimeout(handleResize, 500);
        }

        window.addEventListener('resize', debounce);
        return () => window.removeEventListener('resize', debounce);
    }, []);

    return windowDimensions;
}