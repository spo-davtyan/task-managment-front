
import React from "react";
import { Provider } from "react-redux";
import store from "./redux/store";
import { ToastContainer, toast } from 'react-toastify';
import Wrapper from "./pages/Wrapper";
import 'react-toastify/dist/ReactToastify.css';
import WebSocketProvider from './helpers/socket';
toast.configure();

function App() {
  return (
    <Provider store={store}>
      <ToastContainer limit={1} />
      <WebSocketProvider>
        <Wrapper />
      </WebSocketProvider>
    </Provider>
  );
}

export default App;