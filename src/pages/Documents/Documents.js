import './Documents.css'
import { Button } from "@material-ui/core";


import { Link, useParams } from "react-router-dom";
import ReactHtmlParser from "html-react-parser";
import { createPage, loadProjectPages, loadPage, updatePageContent, updatePageTitle, removePage } from '../../redux/page/actions';
import { connect } from 'react-redux';
import { useHistory } from "react-router-dom";
import { getPages, getPage } from '../../redux/page/selector';
import { useEffect, useState } from 'react';
import { Edit, Close } from '@material-ui/icons';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import './../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import CreateTask from "./CreateTask";
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import { getProject, getProjectMembers } from '../../redux/project/selector';
import { loadProject } from '../../redux/project/actions';

function Documents({ pages, page, project, members, addPage, deletePage, loadPage, loadPages, loadProjectData, updatePageBody, updatePageName }) {
    const history = useHistory()
    const { projectId, docId, isEdit } = useParams()
    const [name, setName] = useState('');

    const [content, setContent] = useState('');

    const onEditorStateChange = (editorState) => {
        setContent(editorState);
    };

    useEffect(() => {
        loadPages(projectId)
        loadProjectData(projectId)
    }, [])
    useEffect(() => {
        if (docId) {
            loadPage(docId)
        }
    }, [docId])
    useEffect(() => {
        if (page) {
            setName(page.title)
            const contentBlock = htmlToDraft(page.content);
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            setContent(editorState)
        }
    }, [page])

    const addPageHandler = () => {
        addPage(projectId).then((page) => {
            history.push(`/${projectId}/docs/${page.id}`)
        })
    }

    const handleOnblur = ({ target }) => {
        const value = target.value.trim()
        if (value !== '') {
            updatePageName(page.id, value)
        } else {
            setName(page.title)
        }
    }

    const handleOnChange = ({ target }) => {
        const { value } = target
        setName(value)
    }

    const removePageHandler = (pageId) => {
        deletePage(pageId)
    }
    const mentions = members.map((member) => {
        return { text: member.name, value: member.name, url: `/users/${member.id}` }
    })
    const isEditMode = isEdit === 'edit'

    return (<div className="Documents">
        <div className="Sidebar">
            {!!pages.length && pages.map((page) => {
                return (<div key={page.id} className="Page">
                    <Link to={`/${projectId}/docs/${page.id}/view`}>{page.title}</Link>
                    <div>
                        <Link to={`/${projectId}/docs/${page.id}/edit`}><Edit fontSize="small" /></Link>
                        <Close fontSize="small" onClick={() => removePageHandler(page.id)} />
                    </div>
                </div>)
            })}
            <Button variant="contained" color="primary"
                onClick={addPageHandler}> Add Page </Button>
        </div>
        <div className="CKEditorWrapper">
            {isEditMode && page && <>
                <div className="PageTitle">
                    <input
                        label="Title" value={name} className='ColumnName'
                        onBlur={handleOnblur} onChange={handleOnChange} autoComplete='off' />
                </div>
                <Editor
                    editorState={content}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor"
                    onEditorStateChange={onEditorStateChange}
                    toolbarCustomButtons={[<CreateTask project={project} />]}
                    // plugins={[addLinkPluginPlugin]}
                    mention={{
                        separator: ' ',
                        trigger: '@',
                        suggestions: mentions,
                    }}
                    onBlur={(event, editorState) => {
                        const output = draftToHtml(convertToRaw(editorState.getCurrentContent()))
                        updatePageBody(page.id, output)
                    }}
                />
            </>}
            {!isEditMode && page && ReactHtmlParser(page.content)}
        </div>
    </div>)
}
const state = store => ({
    pages: getPages(store),
    page: getPage(store),
    members: getProjectMembers(store),
    project: getProject(store)
})
const func = dispatch => ({

    updatePageBody: (pageId, content) => dispatch(updatePageContent(pageId, content)),
    updatePageName: (pageId, title) => dispatch(updatePageTitle(pageId, title)),
    deletePage: (pageId) => dispatch(removePage(pageId)),
    addPage: (projectId) => dispatch(createPage(projectId)),
    loadPage: (pageId) => dispatch(loadPage(pageId)),
    loadPages: (projectId) => dispatch(loadProjectPages(projectId)),
    loadProjectData: (id) => dispatch(loadProject(id)),
})

export default connect(state, func)(Documents)