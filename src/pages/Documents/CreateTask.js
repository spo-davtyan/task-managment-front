import React, { Component, useState } from 'react';
import { EditorState, Modifier, RichUtils } from 'draft-js';

import "./CreateTask.css";
import { createTask } from '../../redux/project/actions';

export const CreateTask = ({ editorState, onChange, project, createNewTask }) => {


    // Getting variables to know text selection 
    let selectionState = editorState.getSelection();
    let anchorKey = selectionState.getAnchorKey();
    console.log('anchorKey', anchorKey);
    let currentContent = editorState.getCurrentContent();

    let currentContentBlock = currentContent.getBlockForKey(anchorKey);
    let start = selectionState.getStartOffset();
    let end = selectionState.getEndOffset();
    let selectedText = currentContentBlock.getText().slice(start, end);


    const addTask = (columnId) => {
        console.log('addTask');
        createTask(project.id, columnId, selectedText).then((task) => {
            console.log('task', task);
            const selection = editorState.getSelection();
            const link = `/project/${project.id}/task/${task.id}`;
            if (!link) {
                onChange(RichUtils.toggleLink(editorState, selection, null));
                return "handled";
            }
            const content1 = editorState.getCurrentContent();
            console.log(content1);
            const contentWithEntity = content1.createEntity("LINK", "MUTABLE", {
                url: link
            });
            const newEditorState = EditorState.push(
                editorState,
                contentWithEntity,
                "create-entity"
            );
            const entityKey = contentWithEntity.getLastCreatedEntityKey();
            onChange(RichUtils.toggleLink(newEditorState, selection, entityKey));
            return "handled";


        })
    };
    const isActive = !editorState.getSelection().isCollapsed()

    const [isOpen, setOpen] = useState(false)
    const openPopup = () => {
        setOpen(!isOpen)
    }
    if (!isActive) {
        if (isOpen) {
            setOpen(false)
        }
    }
    return (<div className="AddTaskWrapper">
        <div className={isActive ? 'active' : 'disabled'} onClick={openPopup}>CreateTask</div>
        <div className={`Popup ${isOpen ? 'open' : ''}`}>
            {project && project.columns.length && project.columns.map(col => {
                return <div className="ColName" key={col.id} onClick={() => {
                    addTask(col.id)
                    setOpen(false)
                }}> {col.name}</div>
            })}
        </div>
    </div>);
}


export default CreateTask