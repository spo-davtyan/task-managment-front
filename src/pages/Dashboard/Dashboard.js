import { Box } from '@material-ui/core';
import Footer from '../../elements/Footer';
import Header from '../../elements/Header';
import { makeStyles } from "@material-ui/core";
const useStyle = makeStyles({
    box: {
        'display': 'flex',
        'flex-direction': 'column',
    }
})
function Dashboard(props) {
    const classes = useStyle()
    return <Box className={classes.box} >
        <Header />
        {props.children}
        <Footer />
    </Box>
}
export default Dashboard