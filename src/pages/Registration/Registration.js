import React, { useState } from "react";
import Copyright from "../../elements/Copyright";
import { toast } from 'react-toastify';
import { makeStyles } from '@material-ui/core/styles';
import { AccessAlarm } from '@material-ui/icons';
import { Button, Box, Container, Typography, Avatar, CssBaseline, TextField } from '@material-ui/core';
import { sendEmail } from "../../redux/user/actions";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function Registration() {
    const classes = useStyles();

    const [email, setEmail] = useState('')

    const _sendEmail = (event) => {
        event.preventDefault()
        sendEmail(email)
            .then(() => toast.success('Please check your mail'))
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <AccessAlarm />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Register Email
                </Typography>
                <form className={classes.form} noValidate onSubmit={_sendEmail}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Email Address"
                        name="email"
                        autoComplete="email"
                        autoFocus
                        onChange={({ target }) => {
                            setEmail(target.value)
                        }}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Send Email
                    </Button>
                </form>
            </div>
            <Box mt={8}>
                <Copyright />
            </Box>
        </Container>
    );
}

export default Registration