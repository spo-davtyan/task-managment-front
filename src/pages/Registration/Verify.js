import React, { useState } from "react";
import Copyright from "../../elements/Copyright";
import { makeStyles } from '@material-ui/core/styles';
import { AccessAlarm } from '@material-ui/icons';
import { Button, Box, Container, Typography, Avatar, CssBaseline, TextField } from '@material-ui/core';
import { verifyAccount } from "../../redux/user/actions";
import { useParams, useHistory } from 'react-router-dom'
import { connect } from "react-redux";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));


function Verify({ verify }) {

    const history = useHistory()
    const { email, token } = useParams() // projectId
    const [name, setName] = useState()
    const [password, setPassword] = useState()

    const classes = useStyles();

    const _verifyAccount = (event) => {
        event.preventDefault()
        verify(email, token, name, password).then(() => {
            history.push('/home')
        })
    }
    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <AccessAlarm />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Create Account
                </Typography>
                <form className={classes.form} noValidate onSubmit={_verifyAccount}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="name"
                        label="Name"
                        name="name"
                        value={name}
                        autoComplete="name"
                        autoFocus
                        onChange={({ target }) => {
                            setName(target.value)
                        }}
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        value={password}
                        id="password"
                        label="Password"
                        name="password"
                        autoComplete="password"
                        onChange={({ target }) => {
                            setPassword(target.value)
                        }}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Create Account
                    </Button>
                </form>
            </div>
            <Box mt={8}>
                <Copyright />
            </Box>
        </Container>
    )
}
const func = dispatch => ({
    verify: (email, token, name, password) => dispatch(verifyAccount(email, token, name, password))
})
export default connect(null, func)(Verify)