import "./Column.css";
import { Button } from "@material-ui/core";
import Tasks from '../../Tasks/Tasks';
import Header from "./Header";
import { updateColumnName } from "../../../../redux/project/actions";

export const Column = ({ provided, item, deleteColumn, addTask, height }) => {
    return <div
        className='Column'
        ref={provided.innerRef}
        {...provided.draggableProps}
    >
        <Header deleteColumn={deleteColumn}
            updateColumnName={updateColumnName}
            columnName={item.name}
            columnId={item.id}
            dragHandleProps={provided.dragHandleProps} />

        <Tasks
            tasks={item.tasks}
            columnId={item.id}
            height={height}
        />
        <Button variant="contained" color="primary" onClick={() => {
            addTask(item)
        }}>
            Add Task
        </Button>
        {provided.placeholder}

    </div>
}

export default Column