import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { CloseTwoTone, DragIndicator } from '@material-ui/icons';
import "./Header.css";
function Header({ deleteColumn, updateColumnName, columnName, columnId, dragHandleProps }) {
    const { id } = useParams() //projectId

    const [name, setName] = useState(columnName);

    const handleOnblur = ({ target }) => {
        const value = target.value.trim()
        if (value !== '') {
            updateColumnName(id, columnId, value)
        } else {
            setName(columnName)
        }
    };
    const handleOnChange = ({ target }) => {
        const { value } = target
        setName(value)
    };
    useEffect(() => {
        if (columnName !== name) {
            setName(columnName)
        }
        // eslint-disable-next-line
    }, [columnName])

    return <div className='ColumnHeaderWrapper'>
        <div {...dragHandleProps} className='DragIcon'>
            <DragIndicator />
        </div>
        <input
            label="Title" value={name} className='ColumnName'
            onBlur={handleOnblur} onChange={handleOnChange} autoComplete='off' />
        <CloseTwoTone color="secondary" fontSize="small" className='RemoveIcon hover' onClick={() => {
            deleteColumn(columnId)
        }} />
    </div>
}

export default Header