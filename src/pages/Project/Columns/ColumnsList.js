import "./ColumnsList.css";
import { Button } from "@material-ui/core";
import Column from "./column/Column";
import { Droppable, Draggable } from "react-beautiful-dnd";
import { createColumn, createTask, deleteColumn } from "../../../redux/project/actions";
import {
    useParams
} from "react-router-dom";
import useWindowDimensions from "../../../helpers/useWindowDimensions ";

const ColumnsList = ({ items }) => {

    const { width, height } = useWindowDimensions()
    const { id } = useParams() //projectId
    const addTask = (column) => {
        createTask(id, column.id)
    }
    const _deleteColumn = (columnId) => {
        deleteColumn(id, columnId)
    }
    const addColumn = () => {
        createColumn(id)
    }
    const columnListWrapperStyle = {
        width: width - 20,
        height: height - 132,
    }
    return (<div className='ColumnListWrapper'
        style={columnListWrapperStyle}>
        <Droppable droppableId="droppable" type="columns" direction='horizontal'>
            {(provided) => (
                <div
                    ref={provided.innerRef}
                    className={`ColumnWrapper`}
                >
                    {items.map((item, index) => (
                        <Draggable key={item.id} draggableId={item.id} index={index}>
                            {(provided) => (
                                <Column provided={provided}
                                    item={item}
                                    deleteColumn={_deleteColumn}
                                    addTask={addTask}
                                    height={height} />
                            )}
                        </Draggable>
                    ))}
                    {provided.placeholder}
                    <div>
                        <Button variant="contained" color="primary"
                            onClick={addColumn}>
                            Add Column
                        </Button>
                    </div>

                </div>
            )}
        </Droppable>
    </div>);
}

export default ColumnsList