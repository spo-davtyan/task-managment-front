
import AvatarList from "./../../elements/AvatarList";
import {
    useParams
} from "react-router-dom";
import { useEffect, useState } from 'react';
import { connect } from "react-redux";
import "./Project.css";
import './ProjectHeader.css'
import { updateProjectName } from "../../redux/project/actions";
import { Button } from "@material-ui/core";
import { sendEmail } from "../../redux/user/actions";
import { toast } from "react-toastify";
import ProjectTags from "./ProjectTags";

function ProjectHeader({ users, projectName, tags, _updateProjectName }) {
    const { id } = useParams() //projectId

    const [name, setName] = useState(projectName);
    const [email, setEmail] = useState('');
    const [isValidEmail, setValidEmail] = useState(false);


    const handleOnblur = ({ target }) => {
        const value = target.value.trim()
        if (value !== '') {
            _updateProjectName(id, value)
        } else {
            setName(projectName)
        }
    }

    const handleOnChange = ({ target }) => {
        const { value } = target
        setName(value)
    }

    const validateEmail = (mail) => {
        const re = /\S+@\S+\.\S+/;
        return re.test(String(mail).toLowerCase());
    }

    const handleInvitePeopleOnChange = ({ target }) => {
        const { value } = target

        setValidEmail(validateEmail(value))
        setEmail(value)
    }

    const sendInvitation = () => {
        sendEmail(email).then(() => {
            toast.success(`You invite new member to board  ${name}`)
        })
    }
    useEffect(() => {
        if (projectName !== name) {
            setName(projectName)
        }
        // eslint-disable-next-line
    }, [projectName])

    return (<div className='ProjectHeaderWrapper'>
        <input
            label="Title" value={name} className='ColumnName'
            onBlur={handleOnblur} onChange={handleOnChange} autoComplete='off' />
        <ProjectTags tags={tags} projectId={id} />
        <div className="ProjectMembers">
            <h5>Project Members</h5>

            {users && <AvatarList isShow={false}
                memberOnClick={() => { }} members={users} />}

            <div className="SendEmail">
                <input
                    label="Invite People" placeholder="Invite People" value={email} className='ColumnName'
                    onChange={handleInvitePeopleOnChange} autoComplete='off' />
                <Button color="primary" disabled={!isValidEmail} onClick={sendInvitation}> Send Invitation</Button>
            </div>
        </div>

    </div>)
}


const func = dispatch => ({
    _updateProjectName: (id, name) => dispatch(updateProjectName(id, name))
})

export default connect(null, func)(ProjectHeader)