import { reorderColumn, reorderTask, moveTask } from '../../redux/project/actions';

const findColumn = (columns, sourceColumnId) => {
    return columns.find(col => col.id === sourceColumnId)
}
const reorderColumns = (columns, sourceColumnId, destColumnId, sourceIndex, destIndex) => {
    const sourceColumn = findColumn(columns, sourceColumnId)
    const sourceTasks = sourceColumn.tasks
    const [removed] = sourceTasks.splice(sourceIndex, 1);
    const destColumn = findColumn(columns, destColumnId)
    const destTasks = destColumn.tasks
    destTasks.splice(destIndex, 0, removed);
    return {
        sourceTasks,
        destTasks
    }
}
export const useOrderHook = (project) => {
    return (result) => {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        const sourceIndex = result.source.index;
        const destIndex = result.destination.index;

        if (result.type === "columns") {
            reorderColumn(project, sourceIndex, destIndex);
        } else if (result.type === "tasks") {

            const sourceColumnId = result.source.droppableId;
            const destColumnId = result.destination.droppableId;

            /** In this case subItems are reOrdered inside same Parent */
            if (sourceColumnId === destColumnId) {

                const column = findColumn(project.columns, sourceColumnId)
                reorderTask(project.id, column, sourceIndex, destIndex)

            } else {

                const {
                    sourceTasks,
                    destTasks
                } = reorderColumns(project.columns, sourceColumnId, destColumnId, sourceIndex, destIndex)
                moveTask(project.id, sourceColumnId, sourceTasks, destColumnId, destTasks);
            }
        }
    }
}
