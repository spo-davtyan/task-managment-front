import { connect, useDispatch } from 'react-redux'
import ColumnsList from "./Columns/ColumnsList";
import { DragDropContext } from "react-beautiful-dnd";
import {
    useParams
} from "react-router-dom";
import { getProject, getProjectTags, hasProject } from '../../redux/project/selector';
import { useEffect } from 'react';
import { loadProject } from '../../redux/project/actions';
import { useOrderHook } from "./useOrderHook";
import { WebSocketContext } from '../../helpers/socket';
import { useContext } from 'react';
import "./Project.css";
import ProjectHeader from './ProjectHeader';

function Project({ loadProject, project, isProjectLoaded, projectTags }) {
    const dispatch = useDispatch()
    const { id } = useParams() // projectId
    // const projcet = project

    const ws = useContext(WebSocketContext);
    const columns = isProjectLoaded ? project.columns : []

    const onDragEnd = useOrderHook(project)
    useEffect(() => {
        loadProject(id)
        ws.subscribeToProject(id)
        ws.socket.on('updateProject', (a) => {
            dispatch(JSON.parse(a))
        })

        return () => {
            ws.socket.removeAllListeners("updateProject");
            return ws.unSubscribeAtProject(id)
        }
        // eslint-disable-next-line
    }, [])
    const users = isProjectLoaded ? project.users : []
    const name = isProjectLoaded ? project.name : ''
    return <>
        {isProjectLoaded && <div className='ProjectWrapper'>
            <ProjectHeader users={users} projectName={name} tags={projectTags} />
            <DragDropContext onDragEnd={(result) => {
                onDragEnd(result)
            }}>
                <ColumnsList axis='x' items={columns} />
            </DragDropContext>
        </div>}
    </>
}
const state = state => ({
    isProjectLoaded: hasProject(state),
    project: getProject(state),
    projectTags: getProjectTags(state),
})
const func = dispatch => ({
    loadProject: (ids) => dispatch(loadProject(ids)),
})
export default connect(state, func)(Project)