import { useState } from "react";
import { ChromePicker } from 'react-color';
import { Button } from "@material-ui/core";
import './ProjectTags.css';
import { addProjectTag, removeProjectTag } from "../../redux/project/actions";
import { Close } from "@material-ui/icons";
function ProjectTags({ tags, projectId }) {
    const [isTagsOpen, setTagsOpen] = useState(false);
    const [isPickerOpen, setPickerOpen] = useState(false);
    const [isValid, setValid] = useState(false);
    const [name, setName] = useState('');
    const [color, setColor] = useState('red');


    const onChange = ({ target }) => {
        const { value } = target
        setName(value)
    }
    const onBlur = ({ target }) => {
        const { value } = target
        if (value.trim()) {
            setName(value.trim())
            setValid(true)
        } else {
            setValid(false)
        }
    }

    const createTag = () => {
        setPickerOpen(false)
        setName('')
        addProjectTag(projectId, tags, { name, color })
    }

    const removeHandler = (id) => {
        removeProjectTag(projectId, tags, id)
    }
    return <div className="ProjectTagsWrapper">
        <div onClick={() => {
            setTagsOpen(!isTagsOpen)
        }}>Project Tags</div>
        <div className={`ProjectTags ${isTagsOpen ? 'active' : ''}`}>
            {tags.map(tag => {
                return <div key={tag.id} className="Tag">
                    <span>{tag.name}</span>
                    <div style={{ backgroundColor: tag.color }}></div>
                    <Close fontSize="small" onClick={() => removeHandler(tag.id)} />
                </div>
            })}
            <div className={`AddNewTag ${isPickerOpen ? 'active' : ''}`}>
                <input placeholder="Tag Name" value={name} onBlur={onBlur} onChange={onChange} />
                <div className="ColorPickerButton" style={{ backgroundColor: color }} onClick={() => {
                    setPickerOpen(!isPickerOpen)
                }}>
                </div>

                <Button variant="contained" color="primary" disabled={!isValid} onClick={createTag}> Add New Tag</Button>
                {isPickerOpen && <ChromePicker
                    color={color}
                    onChangeComplete={(res) => {
                        setColor(res.hex)
                    }}
                    disableAlpha={true}
                />}
            </div>
        </div>

    </div>
}

export default ProjectTags