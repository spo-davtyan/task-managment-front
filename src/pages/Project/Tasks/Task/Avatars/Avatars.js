import './Avatars.css'
import { Add } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { getProjectMembers } from "./../../../../../redux/project/selector";
import { useParams } from "react-router-dom";
import { addTaskMember, removeTaskMember } from '../../../../../redux/project/actions';
import AvatarList from "./../../../../../elements/AvatarList";

function Avatars({ members, getProjectMembers, columnId, taskId }) {
    const [isShow, setShowMembers] = useState(false)
    const { id } = useParams() //projectId
    const availableMembers = getProjectMembers(id)
        .filter(user => !members.find(memberID => memberID === user.id))
    const [filteredUsers, setFilteredUsers] = useState(availableMembers)

    const searchMember = ({ target }) => {
        let { value } = target
        value = value.toLowerCase();

        const filtered = availableMembers.filter(item => {
            const condition = (item.email.toLowerCase().indexOf(value) !== -1 || item.name.toLowerCase().indexOf(value) !== -1)
            return condition
        })
        setFilteredUsers(filtered)
    }
    useEffect(() => {
        setFilteredUsers(availableMembers)
        // eslint-disable-next-line
    }, [members.length])
    const addMember = (userId) => {
        addTaskMember(id, columnId, taskId, userId)

    }
    const removeMember = (userId) => {
        removeTaskMember(id, columnId, taskId, userId)
    }
    const membersList = getProjectMembers(id)
        .filter(user => members.find(memberID => memberID === user.id))
    return <>
        <div className='AvatarWrapper' >
            <Add className={`${isShow ? 'rotate' : ''} Add Avatar Icon Action`} onClick={() => {
                setShowMembers(!isShow)
                if (!isShow) {
                    setFilteredUsers(availableMembers)
                }

            }} />
            <AvatarList isShow={isShow} members={membersList} memberOnClick={removeMember} />

        </div>
        {isShow && <div className='Members' >
            <div className="MemberSearch">
                <input placeholder='Find member' onChange={searchMember} />
            </div>
            <div className="MemberList">
                {filteredUsers.map((item) => {
                    return <div key={item.id} className="Member" onClick={() => {
                        addMember(item.id)
                    }}>
                        <div className='Name'>
                            {item.name}<br />
                            {item.email}
                        </div>
                        <div className='Avatar'>
                            {item.name.charAt(0).toUpperCase()}
                        </div>
                    </div>
                })}
            </div>
        </div>}
    </>
}
const state = state => ({ getProjectMembers: (id) => getProjectMembers(state, id) })

export default connect(state, null)(Avatars)