import './Task.css'
import { DragIndicator, CloseTwoTone } from "@material-ui/icons";
import { useEffect, useState } from "react";
import Avatars from './Avatars/Avatars'

export const Task = ({ deleteTask, updateTaskName, columnId, updateTaskDescription, taskName, taskDescription, taskMembers, taskId, dragHandleProps }) => {

    const [name, setName] = useState(taskName);
    const [description, setDescription] = useState(taskDescription);
    useEffect(() => {
        if (taskName !== name) {
            setName(taskName)
        }
        // eslint-disable-next-line
    }, [taskName])
    useEffect(() => {
        if (taskDescription !== description) {
            setDescription(taskDescription)
        }
        // eslint-disable-next-line
    }, [taskDescription])
    const handleOnblur = ({ target }) => {
        const value = target.value.trim()
        if (value !== '') {
            updateTaskName(taskId, value)
        } else {
            setName(taskName)
        }
    };
    const handleOnChange = ({ target }) => {
        const { value } = target
        setName(value)
    };

    const handleDescOnblur = ({ target }) => {
        const value = target.value.trim()
        updateTaskDescription(taskId, value)
    };
    const handleDescOnChange = ({ target }) => {
        const { value } = target
        setDescription(value)
    };
    return <div className='TaskComponent' >
        <div className='TaskComponentHeader' >
            <div {...dragHandleProps} className="DragIcon Action">
                <DragIndicator />
            </div>
            <input
                label="Title" value={name}
                onBlur={handleOnblur} onChange={handleOnChange} />
            <CloseTwoTone color="secondary" fontSize="small" className='RemoveIcon hover Action' onClick={() => {
                deleteTask(taskId)
            }} />
        </div>
        <div className='TaskComponentBody'>

            <div className='TaskDescriptionWrapper'>
                <textarea className="TaskDescription" value={description}
                    onBlur={handleDescOnblur} onChange={handleDescOnChange} />
            </div>
        </div>
        <div className='TaskComponentFooter'>
            <Avatars taskId={taskId} members={taskMembers} columnId={columnId} />
        </div>
    </div>
}

