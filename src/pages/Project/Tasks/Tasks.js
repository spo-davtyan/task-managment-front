import "./Tasks.css";
import { Droppable, Draggable } from "react-beautiful-dnd";
import { deleteTask, updateTaskDescription, updateTaskName } from "../../../redux/project/actions";
import { useParams } from "react-router-dom";
import { Task } from "./Task/Task";


const Tasks = ({ tasks, columnId, height }) => {
    const { id } = useParams() //projectId
    const _deleteTask = (taskId) => {
        deleteTask(id, columnId, taskId)
    }
    const _updateTaskName = (taskId, name) => {
        updateTaskName(id, columnId, taskId, name)
    }
    const _updateTaskDescription = (taskId, description) => {
        updateTaskDescription(id, columnId, taskId, description)
    }
    return (
        <Droppable droppableId={columnId} type={`tasks`}>
            {(provided) => (
                <div
                    ref={provided.innerRef}
                    className='TasksWrapper'
                    style={{
                        height
                    }}
                >
                    {tasks.map((item, index) => (
                        <Draggable key={item.id} draggableId={item.id} index={index}>
                            {(provided) => (
                                <div ref={provided.innerRef}
                                    className='taskWrapper'
                                    {...provided.draggableProps}>

                                    <Task
                                        taskName={item.name}
                                        taskMembers={item.members}
                                        taskDescription={item.description}
                                        taskId={item.id}
                                        columnId={columnId}
                                        deleteTask={_deleteTask}
                                        updateTaskName={_updateTaskName}
                                        updateTaskDescription={_updateTaskDescription}
                                        dragHandleProps={provided.dragHandleProps} />

                                    {provided.placeholder}
                                </div>
                            )}
                        </Draggable>
                    ))}
                    {provided.placeholder}

                </div>
            )}
        </Droppable>
    );
}

export default Tasks