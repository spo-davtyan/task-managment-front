import { connect } from "react-redux";
import { getUser } from '../../redux/user/selector';
import ProjecCard from "./ProjecCard";
import { Container, Box, Typography, Button, CircularProgress, makeStyles } from '@material-ui/core/';
import { createProject, deleteProject, loadUserProjects } from "../../redux/project/actions";
import { getUserProjects } from "../../redux/project/selector";
import { useEffect } from "react";
import { useState } from "react";

const useStyles = makeStyles(() => ({
    container: {
        padding: '10px',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'column'
    },
    buttonWrapper: {
        height: '100px',
        display: 'flex',
        alignItems: 'center'
    },
    box: {
        flex: '1',
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'center',
    },
    list: {
        height: '100%',
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column'
    }
}));


export function Home({ user, projects, loadUserProjects, _createProject, _deleteProject }) {
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        if (user.projects.length) {
            loadUserProjects(user.projects).finally(() => setLoading(false))
        } else {
            setLoading(false)
        }
    }, [user.projects, projects.length, loadUserProjects])

    const classes = useStyles()


    const createProject = () => {
        _createProject(user.projects)
    }
    const deleteProject = (projectId) => {
        _deleteProject(projectId, user.projects)
    }
    return (

        <Container component="main" maxWidth="lg" className={classes.container}>
            <Box className={classes.buttonWrapper}>
                <Button variant="contained"
                    color="primary"
                    onClick={createProject}>
                    Create New Project
                </Button>
            </Box>
            <Box className={classes.box}>
                {isLoading && <CircularProgress />}
                {!isLoading && !!projects.length && projects.map((project) => {
                    return <ProjecCard key={project.id} project={project} deleteProject={deleteProject} />
                })}
                {!isLoading && !projects.length && (<Box className={classes.list}>
                    <Typography variant="h5" component="h2">
                        You have not projects.
                </Typography>
                    <Button color="primary" onClick={createProject}>Create First one</Button>
                </Box>)}
            </Box>
        </Container>
    );
}
const state = state => ({
    user: getUser(state),
    projects: getUserProjects(state)
})

const func = dispatch => ({
    loadUserProjects: (ids) => dispatch(loadUserProjects(ids)),
    _createProject: (userProjects) => dispatch(createProject(userProjects)),
    _deleteProject: (projectId, userProjects) => dispatch(deleteProject(projectId, userProjects))
})
export default connect(state, func)(Home);