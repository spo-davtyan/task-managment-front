import { Card, CardActions, Button, CardMedia, CardContent, Typography, Divider } from '@material-ui/core/';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom'

const useStyles = makeStyles({
    link: {
        color: '#fff',
        cursor: 'pointer',
        'text-decoration': 'none',
        '&:hover': {
            'text-decoration': 'none',
        }
    },
    card: {
        maxWidth: 300,
        margin: "10px 20px",
        transition: "0.3s",
        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
        "&:hover": {
            boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
        }
    },
    media: {
        paddingTop: "56.25%"
    },
    content: {
        textAlign: "left",
        padding: 4 * 3
    },
    divider: {
        margin: `${4 * 3}px 0`
    },
    heading: {
        fontWeight: "bold"
    },
    subheading: {
        lineHeight: 1.8
    },
    avatar: {
        display: "inline-block",
        border: "2px solid white",
        "&:not(:first-of-type)": {
            marginLeft: -4
        }
    }
});

function ProjecCard({ project, deleteProject }) {
    const classes = useStyles()

    return <Link className={classes.link} to={`/project/${project.id}`}>
        <Card className={classes.card}>
            <CardMedia
                className={classes.media}
                image={
                    "https://picsum.photos/640/360"
                }
            />
            <CardContent className={classes.content}>
                <Typography
                    className={"MuiTypography--heading"}
                    variant={"h6"}
                    gutterBottom
                >
                    {project.name}
                </Typography>
                <Divider className={classes.divider} light />
                <Typography
                    className={"MuiTypography--subheading"}
                    variant={"caption"}
                >
                    We are going to learn different kinds of species in nature that live
                    together to form amazing environment.
      </Typography>
                <CardActions>
                    <Button size="small" color="secondary" onClick={(e) => {
                        e.stopPropagation()
                        e.preventDefault()
                        deleteProject(project.id)
                    }}>
                        Delete
        </Button>

                </CardActions>
            </CardContent>
        </Card>
    </Link>
}

export default ProjecCard