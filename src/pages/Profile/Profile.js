import React from 'react';
import { Container } from '@material-ui/core/';
import UpdateAvatar from "./UpdateAvatar";
import UpdateName from "./UpdateName";
import UpdatePassword from "./UpdatePassword";

function Profile() {
    return (
        <Container>
            <UpdateAvatar />
            <UpdateName />
            <UpdatePassword />
        </Container>
    );
}
export default Profile;