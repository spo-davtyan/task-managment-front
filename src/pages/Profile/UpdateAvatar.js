import { Button, Grid, Box, Typography } from '@material-ui/core/';
import { useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { updateUserAvatar } from '../../redux/user/actions';
import { getUser } from '../../redux/user/selector';
import { getAvatarUrl } from "../../helpers/url";
function UpdateAvatar({ user, updateAvatar }) {
    const [file, setFile] = useState()
    const [file1, setFile1] = useState(null)
    const [hasAvatar, setAvatar] = useState(user.hasAvatar)
    const updateProfile = (event) => {
        event.preventDefault()
        if (file1) {
            const bodyFormData = new FormData();
            bodyFormData.append('avatar', file1);
            updateAvatar(bodyFormData).then(() => {
                toast.success('Good job')
                setAvatar(hasAvatar + 1)
            })
        }


    }

    return <Box>
        <Typography variant="h6" gutterBottom>
            Avatar {hasAvatar && <div> <img width="50" alt="avatar" src={`${getAvatarUrl(user.id)}?v=${hasAvatar}`} /></div>}
        </Typography>
        <form noValidate onSubmit={updateProfile}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <input
                        id="firstName"
                        type="file"
                        name="firstName"
                        value={file}
                        autoComplete="given-name"
                        onChange={({ target }) => {
                            setFile(target.value)
                            setFile1(target.files[0])
                        }}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        Update Avatar
                        </Button>
                </Grid>
            </Grid>
        </form>
    </Box>

}
const state = state => ({ user: getUser(state) })
const func = dispatch => ({
    updateAvatar: (image) => dispatch(updateUserAvatar(image)),
})
export default connect(state, func)(UpdateAvatar);