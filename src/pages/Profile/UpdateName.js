import { Button, TextField, Grid, Box, Typography } from '@material-ui/core/';
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { updateUserName } from '../../redux/user/actions';
import { getUser } from '../../redux/user/selector';

function UpdateName({ user, updateName }) {
    const [name, setName] = useState('')
    const updateProfile = (evemt) => {
        evemt.preventDefault()
        //TODO: vlidate
        updateName(name).then(() => {
            toast.success('Good job')
        })
    }
    useEffect(() => {
        setName(user.name)
    }, [user.name])
    return <Box>
        <Typography variant="h6" gutterBottom>
            Name
            </Typography>
        <form noValidate onSubmit={updateProfile}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="firstName"
                        name="firstName"
                        label="First name"
                        fullWidth
                        value={name}
                        autoComplete="given-name"
                        onChange={({ target }) => {
                            setName(target.value)
                        }}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        Update Profle
                        </Button>
                </Grid>
            </Grid>
        </form>
    </Box>

}
const state = state => ({ user: getUser(state) })
const func = dispatch => ({
    updateName: (name) => dispatch(updateUserName(name)),
})
export default connect(state, func)(UpdateName);