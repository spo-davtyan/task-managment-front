import { Button, TextField, Grid, Box, Typography } from '@material-ui/core/';
import { useState } from "react";
import { connect } from "react-redux";
import { toast } from "react-toastify";
import { updateUserPassword } from '../../redux/user/actions';

function UpdatePassword({ updatePassword }) {
    const [current, setCurrent] = useState('')
    const [confirm, setConfirm] = useState('')
    const [password, setPassword] = useState('')
    const handleUpdatePassword = (evemt) => {
        evemt.preventDefault()
        //TODO: vlidate
        updatePassword(current, confirm, password).then(() => {
            toast.success('Good job')
        })
    }

    return <Box>
        <Typography variant="h6" gutterBottom>
            Password
            </Typography>
        <form noValidate onSubmit={handleUpdatePassword}>
            <Grid container spacing={3}>
                <Grid item xs={12} sm={6}>
                    <TextField
                        required
                        id="current"
                        name="current"
                        type="password"
                        label="Current Password"
                        fullWidth
                        value={current}
                        onChange={({ target }) => {
                            setCurrent(target.value)
                        }}
                    />
                    <TextField
                        required
                        id="password"
                        name="password"
                        type="password"
                        label="New Password"
                        fullWidth
                        value={password}
                        onChange={({ target }) => {
                            setPassword(target.value)
                        }}
                    />
                    <TextField
                        required
                        id="confirm"
                        name="confirm"
                        type="password"
                        label="Confirm Password"
                        fullWidth
                        value={confirm}
                        onChange={({ target }) => {
                            setConfirm(target.value)
                        }}
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                    >
                        Update Password
            </Button>
                </Grid>
            </Grid>
        </form>
    </Box>
}
const func = dispatch => ({
    updatePassword: (current, confirm, password) => dispatch(updateUserPassword(current, confirm, password)),
})
export default connect(null, func)(UpdatePassword);