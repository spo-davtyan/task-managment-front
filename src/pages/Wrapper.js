import React from 'react'
import { connect } from "react-redux";
import {
    BrowserRouter,
    Switch,
    Redirect,
    Route
} from "react-router-dom";
import { isLoggedin } from '../redux/user/selector';
import { Home } from './Home'
import { Login } from './Login'
import { Profile } from './Profile'
import { Project } from './Project'
import Documents from "./Documents/Documents";
import PraviteRoute from "./../helpers/PraviteRoute";
import Registration from './Registration/Registration';
import Verify from './Registration/Verify';

export function Wrapper(props) {
    return <BrowserRouter>
        <Switch>
            <PraviteRoute path="/home" Page={Home} />
            <PraviteRoute path="/profile" Page={Profile} />

            <PraviteRoute path="/project/:id" Page={Project} />
            <PraviteRoute path="/:projectId/docs/:docId?/:isEdit?" Page={Documents} />
            <Route path="/checkEmail" render={() => props.isLoggedin ? <Redirect to="/home" /> : <Registration />} />
            <Route path="/verify/:email/:token" render={() => props.isLoggedin ? <Redirect to="/home" /> : <Verify />} />
            <Route path="/" render={() => props.isLoggedin ? <Redirect to="/home" /> : <Login />} />
        </Switch>
    </BrowserRouter>
}

const state = state => ({ isLoggedin: isLoggedin(state) })

export default connect(state, null)(Wrapper);